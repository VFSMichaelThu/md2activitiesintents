package com.example.michaelthu.myapplication2;

/**
 * Created by michaelthu on 2016-11-15.
 */

public interface Stealable {

    public boolean isLocked();

    public String findCost();
}
